import pywikibot
import time
import re

import urllib.request as ulrlib
from bs4 import BeautifulSoup


def insert_regex_from_list(regex, info_dict, key):
    if len(regex) != 0:
        info_dict[key] = regex[0]
    else:
        info_dict[key] = ''


class EnWikiParser:
    def __init__(self):
        self.site = pywikibot.Site('en', 'wikipedia')

    def _is_uk_in_interwiki(self, page, page_name):
        interwiki = [item.site.lang for item in page.iterlanglinks()]
        if 'uk' in interwiki:
            print('Стаття {} знайдена в УкрВікі за інтервікі'.format(page_name))
            return True

    def _get_page_text(self, movie_name, year, info_dict):
        en_names = [
            movie_name + ' (film, ' + year + ')',
            movie_name + ' (film ' + year + ')',
            movie_name + ' (' + year + ' film)',
            movie_name + ' (film)',
            movie_name
        ]
        for page_name in en_names:
            page = pywikibot.Page(self.site, page_name)
            if self._is_uk_in_interwiki(page, page_name):
                return
            text = page.text
            if text != '' and '#REDIRECT' not in text:
                info_dict['en_wiki_name'] = page_name
                return text

    def _get_uk_category(self, category_name):
        category_name = category_name.replace('|', '')
        category_name = category_name.replace(' ', '_')
        try:
            soup = BeautifulSoup(ulrlib.urlopen('https://en.wikipedia.org/wiki/Category:' + category_name))
            for el in soup.select('li.interlanguage-link > a'):
                if el.get('lang') == 'uk':
                    return el.get('title').replace(' – Ukrainian', '')
        except:
            pass

    def get_uk_page_categories(self, text):
        categories = re.findall('\[\[Category:(.*)\]\]', text)
        uk_categories = list()
        for category_name in categories:
            # category = pywikibot.Category(self.site, category_name)
            uk_category = self._get_uk_category(category_name)
            if uk_category:
                uk_categories.append(uk_category)
        return uk_categories

    def _get_page_info(self, text, info_dict):
        insert_regex_from_list(re.findall('\{\{Official website\|(.*)\}\}', text), info_dict, 'Official website')
        insert_regex_from_list(re.findall('\{\{IMDb title\|(.*)\}\}', text), info_dict, 'IMDb title')
        insert_regex_from_list(re.findall('\{\{Rotten Tomatoes\|(.*)\}\}', text), info_dict, 'Rotten Tomatoes')
        insert_regex_from_list(re.findall('\{\{Mojo title\|(.*)\}\}', text), info_dict, 'Mojo title')
        budget = re.findall('\| budget\s+=\s(.*)', text)
        if len(budget) > 0:
            info_dict['budget'] = budget[0].replace('million', 'млн.')
        gross = re.findall('\| gross\s+=\s(.*)', text)
        if len(gross) > 0:
            info_dict['gross'] = gross[0].replace('million', 'млн.')
        insert_regex_from_list(re.findall('\| distributor\s+=\s(.*)', text), info_dict, 'distributor')
        info_dict['uk_categories'] = self.get_uk_page_categories(text)
        return info_dict

    def get_movie_info(self, movie_name, year):
        if '[' in movie_name:
            print('Illegal characters in page name {}'.format(movie_name))
            return
        info_dict = dict()
        page_text = self._get_page_text(movie_name, year, info_dict)
        if page_text and page_text != '':
            info_dict = self._get_page_info(page_text, info_dict)
            return info_dict
        else:
            print('Cannot find {} in EngWiki'.format(movie_name))


class HurtomParser:
    def __init__(self):
        pass

    def _is_movie(self, text):
        category = re.findall('Толока.*$\n(.*)', text, flags=re.M)[0]
        if 'Фільми' in category or 'Мультфільми' in category:
            return True

    def _get_page_text_title(self, url):
        try:
            page = ulrlib.urlopen(url)
            soup = BeautifulSoup(page)
            title = soup.title.string
            text = soup.text
            return text, title
        except ulrlib.HTTPError as err:
            if err.code != 404:
                raise
            else:
                return None, None

    def get_page_info(self, url):
        text, title = self._get_page_text_title(url)
        if not title or title in ['Вхід']:
            return
        info_dict = dict()
        info_dict['url'] = url
        if self._is_movie(text):
            self._get_title_info(title, info_dict)
            self._get_text_info(text, info_dict)
            return info_dict

    def _get_title_info(self, title, info_dict):
        insert_regex_from_list(re.findall('\((\d+)\)', title), info_dict, 'рік')
        if '/' in title:
            insert_regex_from_list(re.findall('(.+) \/', title), info_dict, 'назва_українською')
            insert_regex_from_list(re.findall('/ (.+) \(', title), info_dict, 'оригінальна_назва')
            info_dict['український'] = False
        else:
            insert_regex_from_list(re.findall('(.+) \(', title), info_dict, 'назва_українською')
            info_dict['український'] = True
            info_dict['оригінальна_назва'] = ''

    def _get_text_info(self, text, info_dict):
        infos = [
            'Країна',
            'Режисер',
            'Актори',
            'Тривалість',
            'Кінокомпанія',
            'Жанр',
            'Сюжет'
        ]
        for info in infos:
            insert_regex_from_list(re.findall(info + ': (.+)', text), info_dict, info)
            info_dict[info] = info_dict[info][:-1]
            for replace_str in ['<ref name="boi" />', '<ref name=afi/>', '{{plainlist|']:
                info_dict[info] = info_dict[info].replace(replace_str, '')
            info_dict[info] = info_dict[info].replace(' ...', '.')
            info_dict[info] = info_dict[info].replace('...', '.')
            info_dict[info] = info_dict[info].replace(' ..', '.')
            info_dict[info] = info_dict[info].replace('..', '.')
        info_dict['Актори'] = self._process_names(info_dict['Актори'])
        info_dict['Режисер'] = self._process_names(info_dict['Режисер'])

    def _process_names(self, names_to_process):
        names_list = names_to_process.split(', ')
        names_processed = list()
        for name in names_list:
            if '/' in name:
                name_processed = re.findall('(.+)[ ]{0,1}[\/|\(]', name)[0]
            else:
                name_processed = name
            if name != '':
                name_processed = '[[' + name_processed + ']]'
                names_processed.append(name_processed)
        names_processed = '<br/> '.join(names_processed)
        return names_processed


class UkWikiPageCreator:
    def __init__(self):
        self.site = pywikibot.Site('uk', 'wikipedia')
        self.category_mapping = {
            'жахи, фантастика, трилер': 'фантастичний трилер',
            'фантастика, трилер': 'фантастичний трилер'
        }

    def _get_movie_infobox(self, hurtom_info_dict, en_wiki_dict):
        infobox = \
            '{{Фільм' + '\n' + \
            '| українська назва  = ' + hurtom_info_dict['назва_українською'] + '\n' + \
            '| оригінальна назва = ' + hurtom_info_dict.get('оригінальна_назва', '') + '\n' + \
            '| плакат            = ' + '\n' + \
            '| жанр              = ' + hurtom_info_dict['Жанр'] + '\n' + \
            '| режисер           = ' + hurtom_info_dict['Режисер'] + '\n' + \
            '| продюсер          = ' + '\n' + \
            '| сценарист         = ' + '\n' + \
            '| актори            = ' + hurtom_info_dict['Актори'] + '\n' + \
            '| композитор        = ' + '\n' + \
            '| оператор          = ' + '\n' + \
            '| монтаж            = ' + '\n' + \
            '| кінокомпанія      = ' + hurtom_info_dict['Кінокомпанія'] + '\n' + \
            '| дистриб’ютор      = ' + en_wiki_dict.get('distributor', '') + '\n' + \
            '| тривалість        = ' + hurtom_info_dict['Тривалість'] + '\n' + \
            '| мова              = ' + '\n' + \
            '| країна            = ' + hurtom_info_dict['Країна'] + '\n' + \
            '| рік               = ' + hurtom_info_dict['рік'] + '\n' + \
            '| дата виходу       =' + '\n' + \
            '| кошторис          = ' + en_wiki_dict.get('budget', '') + '\n' + \
            '| касові збори      = ' + en_wiki_dict.get('gross', '') + '\n' + \
            '| рейтинг           = ' + '<!-- {{fratings|MPAA=X}} -->\n' + \
            '| imdb_id           = ' + en_wiki_dict.get('IMDb title', '') + '\n' + \
            '| сайт              = ' + en_wiki_dict.get('Official website', '') + '\n' + \
            '| попередній        = ' + '\n' + \
            '| наступний         = ' + '\n' + \
            '}}\n\n'
        return infobox

    def _get_ganre(self, ganre):
        ganres_dict = {
            'спорт, комедія, сімейний': 'комедійний фільм',
            'комедія, бойовик': 'комедійний бойовик',
            'драма, романтичний, спорт': 'романтична драма',
            'комедія': 'комедійний фільм',
            'дитячий, сімейний, комедія, пригодницький': 'пригодницька комедія',
            'дитячий, фентезі, казка': 'пригодницький фільм',
            'комедія, кримінальний': 'кримінальна комедія',
            'драма': 'драма',
            'комедія, музичний': 'музична комедія',
            'кримінальний, комедія': 'кримінальна комедія',
            'військова драма': 'військова драма',
            'військовий, драма': 'військова драма',
            'драматичний триллер + екшн': 'драматичний трилер',
            'дитячий, навчальний': 'дитячий навчальний фільм',
            'фентезі, пригодницький': 'пригодницьке фентезі',
            'вестерн': 'вестерн'
        }
        if u'анімація' in ganre or u'мультфільм' in ganre:
            return u'мультфільм'
        elif u'фантастика' in ganre or 'науково-фантастичний' in ganre:
            return u'фантастичний фільм'
        elif ganre in ganres_dict.keys():
            return ganres_dict[ganre]
        elif 'трилер' in ganre:
            return 'трилер'
        elif 'комедія' in ganre:
            return 'комедійний фільм'
        elif 'драма' in ganre:
            return 'драма'
        elif 'детектив' in ganre:
            return 'детективний фільм'
        elif 'пригоди' in ganre or 'пригодницький' in ganre:
            return 'пригодницький фільм'
        elif 'історичний' in ganre:
            return 'історичний фільм'
        elif 'сімейний' in ganre:
            return 'сімейний фільм'
        elif 'трилер' in ganre:
            return 'трилер'
        elif 'жахи' in ganre:
            return 'фільм жахів'
        elif 'музичний' in ganre:
            return 'мюзикл'
        elif 'еротика' in ganre:
            return 'еротичний фільм'
        elif 'кримінал' in ganre and 'бойовик' in ganre:
            return 'кримінальний бойовик'
        else:
            raise Exception('Не можу розпарсити жанр фільму')

    def _get_categories(self, hurtom_info_dict, en_wiki_dict):
        categories = '[[Категорія:Фільми за алфавітом]]\n'
        if hurtom_info_dict['український']:
            categories += '[[Категорія:Українські фільми {}]]'.format(hurtom_info_dict['рік'])
        else:
            categories += '[[Категорія:Фільми ' + hurtom_info_dict['рік'] + ']]\n'
            for category in en_wiki_dict['uk_categories']:
                categories += '[[' + category + ']]\n'
        return categories

    def _get_references(self, en_wiki_dict):
        references ='== Посилання ==\n'
        for item in ['Official website', 'IMDb title', 'Rotten Tomatoes', 'Mojo title']:
            value = en_wiki_dict.get(item)
            if value:
                references += '* {{' + item + '|' + value + '}}\n'
        return references

    def _create_page_text(self, hurtom_info_dict, en_wiki_dict):
        infobox = self._get_movie_infobox(hurtom_info_dict, en_wiki_dict)
        categories = self._get_categories(hurtom_info_dict, en_wiki_dict)
        page_text = \
            infobox + \
            "'''" + hurtom_info_dict['назва_українською'] + "'''" + '&nbsp;— ' + self._get_ganre(hurtom_info_dict['Жанр']) + \
            ' ' + hurtom_info_dict['рік'] + ' року.\n\n' + \
            '== Сюжет ==\n' + hurtom_info_dict['Сюжет'] + '.\n\n' + \
            self._get_references(en_wiki_dict) + '\n' + \
            '* [{} {} на порталі Гуртом]\n\n'.format(hurtom_info_dict['url'], hurtom_info_dict['назва_українською']) + \
            '== Примітки ==\n{{Примітки}}\n\n' + categories + '\n'
        if not hurtom_info_dict['український']:
            page_text += '[[en:' + en_wiki_dict['en_wiki_name'] + ']]'
        else:
            page_text += '{{Film-stub}}\n{{без інтервікі}}'
        return page_text

    def create_page(self, hurtom_info_dict, en_wiki_dict):
        page_text = self._create_page_text(hurtom_info_dict, en_wiki_dict)
        page = pywikibot.Page(self.site, hurtom_info_dict['назва_українською'])
        page.text = page_text
        page.save('Нова сторінка')
        print('Створена сторінка {} з {}'.format(hurtom_info_dict['назва_українською'], hurtom_info_dict['url']))

    def _is_movie_page_present(self, hurtom_info_dict):
        name = hurtom_info_dict['назва_українською']
        possible_names = [name,
                          '{} (фільм)'.format(name),
                          '{} (фільм, {})'.format(name, hurtom_info_dict['рік'])]
        for movie_name in possible_names:
            page = pywikibot.Page(self.site, movie_name)
            if page.text != '':
                print('Стаття {} вже існує у Вікіпедії: {}'.format(movie_name, hurtom_info_dict['url']))
                return True


class Process:
    def __init__(self):
        self.hurtom_parser = HurtomParser()
        self.en_wiki_parser = EnWikiParser()
        self.wiki_page_creator = UkWikiPageCreator()

    def create_page(self, url):
        hurtom_info_dict = self.hurtom_parser.get_page_info(url)
        if not hurtom_info_dict:
            return
        if hurtom_info_dict['назва_українською'] == '':
            print('Не вдалося розпарсити назву')
            return
        if '[' in hurtom_info_dict['назва_українською']:
            print('Illegal characters in page name {}'.format(hurtom_info_dict['назва_українською']))
            return
        if not self.wiki_page_creator._is_movie_page_present(hurtom_info_dict):
            if hurtom_info_dict['український']:
                en_wiki_info_dict = dict()
            else:
                en_wiki_info_dict = self.en_wiki_parser.get_movie_info(hurtom_info_dict['оригінальна_назва'],
                                                                       hurtom_info_dict['рік'])
            if en_wiki_info_dict or hurtom_info_dict['український']:
                self.wiki_page_creator.create_page(hurtom_info_dict, en_wiki_info_dict)

    def main(self):
        index = 7211
        while index < 7500:
            try:
                url = 'https://toloka.to/t{}'.format(index)
                print(url)
                self.create_page(url)
                index += 1
            except Exception as e:
                print(type(e))
                time.sleep(5)


if __name__ == '__main__':
    Process().main()

